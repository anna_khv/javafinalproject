package Util;

public class ShiftArray {
    private ShiftArray() {
        throw new IllegalArgumentException("this is static class");
    }

    public static void shiftArray(Object[] objectArray, int deleteIndex, int lastUsedIndex) {
        System.out.println("before shifting");
        for (int i = 0; i <=lastUsedIndex ; i++) {
            System.out.println(objectArray[i]);
        }

        for (int i = deleteIndex; i <= lastUsedIndex; i++) {
            objectArray[deleteIndex] = objectArray[++deleteIndex];
        }

        System.out.println("after array elements has been shifted");
        for (int i = 0; i <lastUsedIndex ; i++) {
            System.out.println(objectArray[i]);
        }
    }
}
