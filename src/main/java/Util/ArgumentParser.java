package Util;

import Service.UniManagement;
import pattern.*;

public class ArgumentParser {
    private UniManagement management;
    public static final String CREATE_STUDENT = "createStudent";
    public static final String DELETE_STUDENT = "deleteStudent";
    public static final String CREATE_COURSE = "createCourse";
    public static final String DELETE_COURSE = "deleteCourse";
    public static final String CREATE_ASSISTANT = "createAssistance";
    public static final String DELETE_ASSISTANT = "deleteAssistance";
    public static final String CREATE_FROFESSOR = "createProfessor";
    public static final String DELETE_FROFESSOR = "deleteProfessor";
    public static final String ASIGN_STUDENT_TO_COURSE = "asighStudentToCourse";
    public static final String ASIGN_ASSISTANT_TO_COURSE = "asighAssistanceToCourse";
    public static final String ASIGN_PROFESSOR_TO_COURSE = "asighProfessorToCourse";
    public static final String ADD_STUDENTS_TO_COURSE = "addStudentsToCourse";
    public static final String REMOVE_STUDENT_FROM_COURSE = "removeStudentFromCourse";

    public ArgumentParser(UniManagement management) {
        this.management = management;
    }

    public Command parseArgument(String[] args) {
        Command command = null;
        int id;
        String firstName;
        String lastName;
        String courseName;
        String commandName = args[0];
        switch (commandName) {
            case CREATE_COURSE:
                courseName = args[1];
                command = new CreateCourseCommand(management, courseName);
                System.out.println("create course");
                return command;
            case CREATE_STUDENT:
                id = Integer.parseInt(args[1]);
                firstName = args[2];
                lastName = args[3];
                String facNumber = args[4];
                command = new CreateStudentCommand(management, id, firstName, lastName, facNumber);
                System.out.println("create student");
                return command;
            case CREATE_ASSISTANT:
                id = Integer.parseInt(args[1]);
                firstName = args[2];
                lastName = args[3];
                command = new CreateAssistantCommand(management, firstName, lastName, id);
                System.out.println("create assistant");
                return command;
            case CREATE_FROFESSOR:
                id = Integer.parseInt(args[1]);
                firstName = args[2];
                lastName = args[3];
                command = new CreateProfessorCommand(management, firstName, lastName, id);
                System.out.println("create professor");
                return command;
            case DELETE_ASSISTANT:
                id = Integer.parseInt(args[1]);
                command = new DeleteAssistantCommand(management, id);
                System.out.println("delete assistant");
                return command;
            case DELETE_COURSE:
                courseName = args[1];
                command = new DeleteCourseCommand(management, courseName);

                System.out.println("delete course");
                return command;
            case DELETE_FROFESSOR:
                id = Integer.parseInt(args[1]);
                command = new DeleteProfessorCommand(management, id);
                System.out.println("delete professor");
                return command;
            case DELETE_STUDENT:
                id = Integer.parseInt(args[1]);
                command = new DeleteStudentCommand(management, id);
                System.out.println("deleting student");
                return command;
            case ASIGN_ASSISTANT_TO_COURSE:
                id = Integer.parseInt(args[1]);
                courseName = args[2];
                command = new AsignAssistantToCourseCommand(management, id, courseName);
                System.out.println("assign assistant to course");
                return command;
            case ASIGN_PROFESSOR_TO_COURSE:
                id = Integer.parseInt(args[1]);
                courseName = args[2];
                command = new AsignProfessorToCourseCommand(management, id, courseName);
                System.out.println("assign professor to course");
                return command;
            case ASIGN_STUDENT_TO_COURSE:
                id = Integer.parseInt(args[1]);
                courseName = args[2];
                command = new AddStudentToCourseCommand(management, id, courseName);
                System.out.println("assign student to course");
                return command;
            case ADD_STUDENTS_TO_COURSE:
                courseName = args[args.length - 1];
                int[] students = new int[args.length - 2];
                for (int i = 0; i < students.length; i++) {
                    students[i] = Integer.parseInt(args[i + 1]);
                }
                command = new AddStudentsToCourseCommand(management, students, courseName);
                System.out.println("add students to course");
                return command;
            case REMOVE_STUDENT_FROM_COURSE:
                id = Integer.parseInt(args[1]);
                courseName = args[2];
                command = new RemoveStudentFromCourseCommand(management, id, courseName);
                System.out.println("remove student from course");
                return command;
            default:
                throw new IllegalArgumentException(args[0] + " command is not valid");
        }

    }

}
