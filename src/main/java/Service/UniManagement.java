package Service;

import Pojo.Course;
import Pojo.Lecturer;
import Pojo.Student;

public interface UniManagement {

    public Course createCourse(String courseName);

    public boolean deleteCourse(String courseName);

    public Student createStudent(int id, String firstName, String lastName, String
            facNumber);

    public boolean deleteStudent(int id);

    public Lecturer createAssistant(int id, String firstName, String lastName);

    public boolean deleteAssistant(int id);

    public Lecturer createProfessor(int id, String firstName, String lastName);

    public boolean deleteProfessor(int id);

    public boolean asignAssistantToCourse(Lecturer assistance, Course course);

    public boolean asignProfessorToCourse(Lecturer professor, Course course);

    public boolean addStudentToCourse(Student student, Course course);

    public boolean addStudentsToCourse(Student[] students, Course course);

    public boolean removeStudentFromCourse(Student student, Course course);

}
