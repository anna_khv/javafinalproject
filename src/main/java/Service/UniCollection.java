package Service;

import Pojo.Course;
import Pojo.Lecturer;
import Pojo.Student;

public interface UniCollection {
    public Student[] getAllStudents();

    public Student getStudentById(int id);

    public Course[] getAllCourses();

    public Course getCourseByName(String name);

    public Lecturer[] getAssistants();

    public Lecturer getAssistantById(int id);

    public Lecturer[] getMainLecturers();

    public Lecturer getProfById(int id);
}
