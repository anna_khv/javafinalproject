package Service;

import Pojo.Course;
import Pojo.Lecturer;
import Pojo.LecturerType;
import Pojo.Student;
import Util.ShiftArray;

public class UniManagementImp implements UniManagement, UniCollection {

    private Student[] allStudents;
    private Course[] allCourses;
    private Lecturer[] assistants;
    private Lecturer[] mainLecturers;

    private int lastUsedStudentIndex;
    private int lastUsedCourseIndex;
    private int lastUsedAssistantIndex;
    private int lastUsedMainLecturerIndex;

    {
        System.out.println("initialising instance variables");
        allStudents = new Student[10];
        allCourses = new Course[1000];
        assistants = new Lecturer[10];// initial capacity
        mainLecturers = new Lecturer[10];// initial capacity

        lastUsedStudentIndex = -1;
        lastUsedCourseIndex = -1;
        lastUsedAssistantIndex = -1;
        lastUsedMainLecturerIndex = -1;
    }

    public Student[] getAllStudents() {
        return allStudents;
    }

    @Override
    public Student getStudentById(int id) {
        for (Student stud : allStudents) {
            if (stud!=null && stud.getId() == id) return stud;
        }
        return null;
    }


    public Course[] getAllCourses() {
        return allCourses;
    }

    @Override
    public Course getCourseByName(String name) {
        for (Course course : allCourses) {
            if (course!=null && course.getName().equals(name)) return course;
        }
        return null;
    }


    public Lecturer[] getAssistants() {
        return assistants;
    }

    @Override
    public Lecturer getAssistantById(int id) {
        for (Lecturer assist : assistants) {
            if (assist!=null && assist.getId() == id) return assist;
        }
        return null;
    }


    public Lecturer[] getMainLecturers() {
        return mainLecturers;
    }

    @Override
    public Lecturer getProfById(int id) {
        for (Lecturer prof : mainLecturers) {
            if (prof!=null && prof.getId() == id) return prof;
        }
        return null;
    }


    @Override
    public Course createCourse(String courseName) {
        for (Course cours : allCourses) {
            if (cours != null && cours.getName().equals(courseName.toLowerCase())) {
                throw new IllegalArgumentException("the course with given name already exists");
            }
        }
        Course course = new Course(courseName);
        allCourses[++lastUsedCourseIndex] = course;
        return course;
    }

    @Override
    public boolean deleteCourse(String courseName) {
        for (int i = 0; i < allCourses.length; i++) {
            if (allCourses[i] != null && allCourses[i].getName().equals(courseName.toLowerCase())) {
                ShiftArray.shiftArray(allCourses, i, lastUsedCourseIndex);
                lastUsedCourseIndex--;
                return true;
            }

        }
        return false;
    }

    @Override
    public Student createStudent(int id, String firstName, String lastName, String facNumber) {
        Student student = new Student(id, firstName, lastName, facNumber);
        for (Student st : allStudents) {
            if (st != null && st.equals(student)) {
                throw new IllegalArgumentException("the student with given name already exists"); // although it is possible for two persons to have same name and surname
            } else if (st != null && st.getId() == student.getId()) {
                throw new IllegalArgumentException("the student with given id already exists, please try another id");
            }
        }

        allStudents[++lastUsedStudentIndex] = student;
        return student;


    }

    @Override
    public boolean deleteStudent(int id) {
        for (int i = 0; i < allStudents.length; i++) {
            if (allStudents[i] != null && allStudents[i].getId() == id) {
                ShiftArray.shiftArray(allStudents, i, lastUsedStudentIndex);
                lastUsedStudentIndex--;
                return true;
            }

        }
        return false;
    }

    @Override
    public Lecturer createAssistant(int id, String firstName, String lastName) {

        Lecturer assisstant = new Lecturer(id, firstName, lastName, LecturerType.ASSISTANT);
        for (Lecturer assist : assistants) {
            if (assist != null && assist.equals(assisstant)) {
                throw new IllegalArgumentException("the assisstant with given name already exists");
            } else if (assist != null && assist.getId() == assisstant.getId()) {
                throw new IllegalArgumentException("assistant with given id already exists , please try different id");
            }
        }
        if (assistants[assistants.length - 1] != null) {
            Lecturer[] doubledArray = doubleCapacity(assistants);
            assistants = doubledArray;
        }
        assistants[++lastUsedAssistantIndex] = assisstant;
        return assisstant;
    }

    @Override
    public boolean deleteAssistant(int id) {
        for (int i = 0; i < assistants.length; i++) {
            if (assistants[i] != null && assistants[i].getId() == id) {
                ShiftArray.shiftArray(assistants, i, lastUsedAssistantIndex);
                lastUsedAssistantIndex--;
                return true;
            }

        }
        return false;
    }

    @Override
    public Lecturer createProfessor(int id, String firstName, String lastName) {

        Lecturer main = new Lecturer(id, firstName, lastName, LecturerType.PROFESSOR);
        for (Lecturer prof : mainLecturers) {
            if (prof != null && prof.equals(main)) {
                throw new IllegalArgumentException("the professor with given name already exists");
            } else if (prof != null && prof.getId() == main.getId()) {
                throw new IllegalArgumentException("this professor with given id already exists please try different id");
            }
        }
        if (mainLecturers[mainLecturers.length - 1] != null) {
            Lecturer[] doubledArray = doubleCapacity(mainLecturers);
            mainLecturers = doubledArray;
        }
        mainLecturers[++lastUsedMainLecturerIndex] = main;
        return main;
    }

    @Override
    public boolean deleteProfessor(int id) {
        for (int i = 0; i < mainLecturers.length; i++) {
            if (mainLecturers[i] != null && mainLecturers[i].getId() == id) {
                ShiftArray.shiftArray(mainLecturers, i, lastUsedMainLecturerIndex);
                lastUsedMainLecturerIndex--;
                return true;
            }

        }
        return false;
    }

    @Override
    public boolean asignAssistantToCourse(Lecturer assistance, Course course) {
        course.setAssistant(assistance);
        return true;
    }

    @Override
    public boolean asignProfessorToCourse(Lecturer professor, Course course) {
        course.setMainLecturer(professor);
        return true;
    }

    @Override
    public boolean addStudentToCourse(Student student, Course course) {
        course.addStudent(student);
        student.addCourse(course);
        return true;
    }

    @Override
    public boolean addStudentsToCourse(Student[] students, Course course) {
        for (Student student : students) {
            addStudentToCourse(student, course);
        }
        return true;
    }

    @Override
    public boolean removeStudentFromCourse(Student student, Course course) {
        course.deleteStudent(student);
        student.deleteCourse(course);
        return true;
    }

    private Lecturer[] doubleCapacity(Lecturer[] array) {
        System.out.println("doubling the capacity");
        Lecturer[] biggerArray = new Lecturer[array.length * 2];
        for (int i = 0; i < array.length; i++) {
            biggerArray[i] = array[i];
        }

        return biggerArray;
    }
}
