package pattern;

import Service.UniManagement;

public class DeleteAssistantCommand implements Command {
    private UniManagement management;
    private int id;

    public DeleteAssistantCommand(UniManagement management, int id) {
        this.management = management;
        this.id = id;
    }

    @Override
    public void execute() {
        management.deleteAssistant(id);

    }
}
