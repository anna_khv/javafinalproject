package pattern;

import Service.UniManagement;

public class CreateCourseCommand  implements Command{
    private UniManagement management;
    private String courseName;


    public CreateCourseCommand(UniManagement management,String courseName) {

        this.management = management;
        this.courseName=courseName;
    }

    @Override
    public void execute() {
         management.createCourse(this.courseName);
    }
}
