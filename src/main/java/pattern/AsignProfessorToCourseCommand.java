package pattern;

import Pojo.Course;
import Pojo.Lecturer;
import Service.UniCollection;
import Service.UniManagement;

public class AsignProfessorToCourseCommand implements Command {
    private UniManagement management;
    private int profId;
    private String courseName;

    public AsignProfessorToCourseCommand(UniManagement management, int profId, String courseName) {
        this.management = management;
        this.profId = profId;
        this.courseName = courseName;
    }

    @Override
    public void execute() {
        UniCollection collection = (UniCollection) management;
        Course course = collection.getCourseByName(courseName);
        Lecturer prof = collection.getProfById(profId);
        management.asignProfessorToCourse(prof, course);
    }
}
