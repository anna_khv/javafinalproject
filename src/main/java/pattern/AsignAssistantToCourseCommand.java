package pattern;

import Pojo.Course;
import Pojo.Lecturer;
import Service.UniCollection;
import Service.UniManagement;

public class AsignAssistantToCourseCommand implements Command {

    private UniManagement management;
    private int idAssistant;
    private String courseName;

    public AsignAssistantToCourseCommand(UniManagement management, int idAssistant, String courseName) {
        this.management = management;
        this.idAssistant = idAssistant;
        this.courseName = courseName;
    }

    @Override
    public void execute() {
        UniCollection collection=(UniCollection) management;
        Lecturer assisstant=collection.getAssistantById(idAssistant);
        Course course=collection.getCourseByName(courseName);
        management.asignAssistantToCourse(assisstant, course);
    }
}
