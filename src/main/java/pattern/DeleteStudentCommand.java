package pattern;

import Service.UniManagement;

public class DeleteStudentCommand implements Command{
    private UniManagement management;
    private int id;

    public DeleteStudentCommand(UniManagement management, int id) {
        this.management = management;
        this.id = id;
    }

    @Override
    public void execute() {
        management.deleteStudent(id);

    }
}
