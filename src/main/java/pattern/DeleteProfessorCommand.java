package pattern;

import Service.UniManagement;

public class DeleteProfessorCommand implements Command {

    private UniManagement management;
    private int id;

    public DeleteProfessorCommand(UniManagement management, int id) {
        this.management = management;
        this.id = id;
    }

    @Override
    public void execute() {
        management.deleteProfessor(id);

    }
}
