package pattern;

public interface Command {
    public void execute();
}
