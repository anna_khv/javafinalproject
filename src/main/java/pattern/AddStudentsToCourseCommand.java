package pattern;

import Pojo.Course;
import Pojo.Student;
import Service.UniCollection;
import Service.UniManagement;

public class AddStudentsToCourseCommand implements Command {
    private UniManagement management;
    private int[] studentIds;
    private String courseName;

    public AddStudentsToCourseCommand(UniManagement management, int[] studentIds, String courseName) {
        this.management = management;
        this.studentIds = studentIds;
        this.courseName = courseName;
    }


    @Override
    public void execute() {
        UniCollection collection = (UniCollection) management;
        Course course = collection.getCourseByName(courseName);
        Student[] students = new Student[studentIds.length];
        for (int i = 0; i < studentIds.length; i++) {
            int id = studentIds[i];
            Student student = collection.getStudentById(id);
            students[i] = student;

        }
        management.addStudentsToCourse(students, course);
    }
}
