package pattern;

import Service.UniManagement;

public class CreateAssistantCommand implements Command {
    private UniManagement management;
    private String firstName;
    private String lastName;
    private int id;

    public CreateAssistantCommand(UniManagement management, String firstName, String lastName, int id) {
        this.management = management;
        this.firstName = firstName;
        this.lastName = lastName;
        this.id = id;
    }

    @Override
    public void execute() {
        management.createAssistant(id, firstName, lastName);
    }
}
