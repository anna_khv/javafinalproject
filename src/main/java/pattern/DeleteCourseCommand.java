package pattern;

import Service.UniManagement;

public class DeleteCourseCommand implements Command{
    private UniManagement management;
    private String courseName;

    public DeleteCourseCommand(UniManagement management, String courseName) {
        this.management = management;
        this.courseName = courseName;
    }

    @Override
    public void execute() {
        management.deleteCourse(courseName);

    }
}
