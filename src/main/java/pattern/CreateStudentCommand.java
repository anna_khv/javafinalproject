package pattern;

import Service.UniManagement;

public class CreateStudentCommand implements Command {
     private UniManagement management;
     private int  id;
     private String firstName;
     private String lastName;
     private String facNumber;

    public CreateStudentCommand(UniManagement management, int id, String firstName, String lastName, String facNumber) {
        this.management = management;
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.facNumber = facNumber;
    }

    @Override
    public void execute() {
        management.createStudent(id,firstName,lastName,facNumber);

    }
}
