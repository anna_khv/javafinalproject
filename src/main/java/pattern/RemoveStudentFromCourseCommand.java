package pattern;

import Pojo.Course;
import Pojo.Student;
import Service.UniCollection;
import Service.UniManagement;

public class RemoveStudentFromCourseCommand implements Command {
    private UniManagement management;
    private int studentId;
    private String courseName;

    public RemoveStudentFromCourseCommand(UniManagement management, int studentId, String courseName) {
        this.management = management;
        this.studentId = studentId;
        this.courseName = courseName;
    }

    @Override
    public void execute() {
        UniCollection collection = (UniCollection) management;
        Course course = collection.getCourseByName(courseName);
        Student student = collection.getStudentById(studentId);
        management.removeStudentFromCourse(student, course);

    }
}
