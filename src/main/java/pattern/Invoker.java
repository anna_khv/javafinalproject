package pattern;

public class Invoker {
   public void setCommand(Command command){
        command.execute();
    }
}
