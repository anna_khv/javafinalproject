package Pojo;

import java.util.Objects;

public class User {
    private final int id;
    private final String firstName;
    private final String lastName;

    public User(int id, String firstName, String lastName) {
        this.id = id;
        this.firstName = firstLetterUpper(firstName);
        this.lastName = firstLetterUpper(lastName);
    }

    public long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    private String firstLetterUpper(String word) {
        String first = word.substring(0, 1).toUpperCase();
        String rest = word.substring(1).toLowerCase();
        String result = first + rest;
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return firstName.equals(user.firstName) && lastName.equals(user.lastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName);
    }
}
