package Pojo;

import Util.ShiftArray;

public class Student extends User {
    private final String facNumber;
    private final Course[] courses;
    private int lastUsedCourseIndex;

    {
        courses = new Course[10];
        lastUsedCourseIndex = -1;
    }

    public Student(int id, String firstName, String lastName, String facNumber) {
        super(id, firstName, lastName);
        this.facNumber = facNumber;

    }

    public String getFacNumber() {
        return facNumber;
    }

    public Course[] getCourses() {
        return courses;
    }

    public void addCourse(Course course) {
        if (courses[courses.length - 1] != null)
            throw new ArrayIndexOutOfBoundsException("this student is already registered to the max number of courses");
        courses[++lastUsedCourseIndex] = course;
    }

    public void deleteCourse(Course course) {
        for (int e = 0; e < courses.length; e++) {
            if (courses[e] != null && courses[e].equals(course)) {
                ShiftArray.shiftArray(courses, e, lastUsedCourseIndex);
                lastUsedCourseIndex--;
            }

        }
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return "Pojo.Student{" +
                "id = " + this.getId() +
                ", firstName = " + this.getFirstName() +
                ", lastName = " + this.getLastName() +
                ", facNumber = " + facNumber +
                '}';
    }
}
