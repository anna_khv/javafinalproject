package Pojo;

import Util.ShiftArray;

import java.util.Objects;

public class Course {
    private final String name;
    private Student[] students;
    private Lecturer mainLecturer;
    private Lecturer assisstant;

    private int lastUsedStudentIndex;
    {
        students = new Student[30];
        lastUsedStudentIndex=-1;
    }
    public Course(String name) {
        this.name = name.toLowerCase();
        mainLecturer = null;
        assisstant = null;
    }

    public Course(String name, Lecturer mainLecturer, Lecturer assistant) {
        this.name = name.toLowerCase();
        setAssistant(assistant);
        setMainLecturer(mainLecturer);

    }

    public String getName() {
        return name;
    }

    public Student[] getStudents() {
        return students;
    }

    public Lecturer getMainLecturer() {
        return mainLecturer;
    }

    public Lecturer getAssisstant() {
        return assisstant;
    }

    public void setAssistant(Lecturer assisstantLecturer) {
        if (assisstantLecturer.getLectorType() == LecturerType.ASSISTANT) {
            assisstant = assisstantLecturer;
        } else {
            throw new IllegalArgumentException("wrong input");
        }
    }

    public void setMainLecturer(Lecturer mainTeacher) {
        if (mainTeacher.getLectorType() == LecturerType.ASSISTANT) {
            throw new IllegalArgumentException("wrong input");
        } else {
            mainLecturer = mainTeacher;
        }
    }

    public void addStudent(Student student) {
        if (students[students.length-1]!=null) throw new ArrayIndexOutOfBoundsException("this course already has max number of students");
        students[++lastUsedStudentIndex] = student;
    }

    public void deleteStudent(Student student) {
        for (int e = 0; e < students.length; e++) {
            if (students[e]!=null && students[e].equals(student)) {
                ShiftArray.shiftArray(students, e, lastUsedStudentIndex);
                lastUsedStudentIndex--;
            }

        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Course course = (Course) o;
        return Objects.equals(name, course.name) && Objects.equals(mainLecturer, course.mainLecturer) && Objects.equals(assisstant, course.assisstant);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, mainLecturer, assisstant);
    }

    private String printNonNullStudents(){
        StringBuilder builder=new StringBuilder();
        for(Student student:students){
            if(student!=null){
                builder.append(student+" ");
            }

        }
        return builder.toString();
    }
    @Override
    public String toString() {
        return "Pojo.Course{" +
                "name='" + name + '\'' +
                ", mainLecturer=" + mainLecturer +
                ", assisstant=" + assisstant +
                ", students=" +"{ "+ printNonNullStudents() +" }" +
                '}';
    }
}
