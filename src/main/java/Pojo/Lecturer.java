package Pojo;

import Pojo.Course;
import Util.ShiftArray;

import java.util.Arrays;

public class Lecturer extends User {
    private final LecturerType lectorType;
    private final Course[] courses;

    private  int lastUsedCourseIndex;
    {
        courses = new Course[4];
        lastUsedCourseIndex=-1;
    }
    public Lecturer(int id, String firstName, String lastName, LecturerType lectorType) {
        super(id, firstName, lastName);
        this.lectorType = lectorType;

    }

    public LecturerType getLectorType() {
        return lectorType;
    }

    public Course[] getCourses() {
        return courses;
    }

    public void addCourse(Course course) {

        if (courses[courses.length-1]!=null) throw new ArrayIndexOutOfBoundsException("this lecturer teacher max number of courses already");
        courses[++lastUsedCourseIndex]=course;
    }

    public void deleteCourse(Course course) {
        for (int i = 0; i <courses.length ; i++) {
            if(courses[i]!=null && courses[i].equals(course)){
                ShiftArray.shiftArray(courses,i,lastUsedCourseIndex);
                lastUsedCourseIndex--;
            }

        }
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return "Pojo.Lecturer{" +
                "id = "+this.getId() +
                ", firstName = "+this.getFirstName() +
                ", lastName = "+this.getLastName() +
                ", lectorType=" + lectorType +
                '}';
    }
}
