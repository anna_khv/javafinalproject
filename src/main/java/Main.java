import Service.UniManagement;
import Service.UniManagementImp;
import Util.ArgumentParser;
import pattern.Command;
import pattern.Invoker;

import java.util.Scanner;

public class Main {


    public static void main(String[] args) {
        UniManagement management = new UniManagementImp();
        ArgumentParser parser = new ArgumentParser(management);
        Scanner scanner = new Scanner(System.in);
        System.out.println(" first write the name of operation that you want to perform next operation parameters =))) " +
                "\n  you can use following operations :" +
                "\n 1. createStudent studentId studentName studentLastName facultyNumber " +
                "\n 2. deleteStudent studentId " +
                "\n 3. createCourse courseName " +
                "\n 4. deleteCourse courseName " +
                "\n 5. createAssistance id firstName lastName " +
                "\n 6. deleteAssistance id " +
                "\n 7. createProfessor id firstName lastName " +
                "\n 8. deleteProfessor id " +
                "\n 9. asighStudentToCourse studentId courseName " +
                "\n 10 asighAssistanceToCourse assistId courseName " +
                "\n 11. asighProfessorToCourse profId courseName " +
                "\n 12. addStudentsToCourse id1, id2, id3 ... courseName " +
                "\n 13. removeStudentFromCourse studentId course"
        );

        String line = "";
        do {
            try {
                System.out.print("please enter operation : ");
                line = scanner.nextLine();
                if(!line.equals("stop")) {
                    String[] input = line.split(" ");
                    Command command = parser.parseArgument(input);
                    Invoker invoker = new Invoker();
                    invoker.setCommand(command);
                }
                System.out.println("if you want to stop program type stop");
            } catch (IllegalArgumentException | NullPointerException | ArrayIndexOutOfBoundsException e) {
                System.out.println(e.getMessage());

            }

        } while (!line.equals("stop"));

        System.out.println("you have finished using our application");
        System.out.println("hope you enjoyed it");
        System.out.println("see you next time ");
    }

}
