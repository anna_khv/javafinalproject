import Pojo.Course;
import Pojo.Lecturer;
import Pojo.Student;
import Service.UniManagement;
import Service.UniManagementImp;

public class Test {

    public static void main(String[] args){
        UniManagement management=new UniManagementImp();
        System.out.println("testing courses");
        Course course= management.createCourse("Literature");
        Course course1= management.createCourse("English");
        Course course2= management.createCourse("Maths");
        Course course3= management.createCourse("science");
        management.deleteCourse("English");
        Course course4= management.createCourse("biology");
        Course course5= management.createCourse("chemistry");
        management.deleteCourse("maths");
        System.out.println("testing student");
        Student student= management.createStudent(1, "Anja", "Komarnizka","2233");
        Student student1= management.createStudent(2, "Sofia", "Smith","2423");
        Student student2= management.createStudent(3, "Maria", "sharapova","2923");
        management.deleteStudent(2);
        Student student3= management.createStudent(4, "Michael", "Jones","2553");
        Student student4= management.createStudent(5, "James", "Jero","6543");
        management.deleteStudent(3);
        Student student6= management.createStudent(6, "Ema", "Watsom","2553");
        Student student7= management.createStudent(7, "Eka", "Beridze","6573");
        Student student8= management.createStudent(8, "Giorgi", "Tarielashvili","2573");
        Student student49= management.createStudent(9, "Ketie", "Fanjikidze","9543");
        System.out.println("testing assistant");
        Lecturer assistant= management.createAssistant(1,"Misha","Butkhuzi");
        Lecturer assistant1= management.createAssistant(2,"Giorgi","Gogelia");
        management.deleteAssistant(2);
        Lecturer assistant2= management.createAssistant(3,"Irma","Shengelia");
        Lecturer assistant3= management.createAssistant(4,"Sasha","Kakabadze");
        Lecturer assistant4= management.createAssistant(5,"alex","Maglakelidze");
        Lecturer assistant5= management.createAssistant(6,"Gia","GIorgishvili");
        Lecturer assistant6= management.createAssistant(7,"Kakha","Meskhi");
        Lecturer assistant7= management.createAssistant(9,"Jack","Ioseliani");
        Lecturer assistant8= management.createAssistant(10,"Sofia","Sagaradze");
        Lecturer assistant9= management.createAssistant(11,"Sofo","Qardava");
        Lecturer assistant10= management.createAssistant(12,"Marina","Mumladze");
        Lecturer assistant11= management.createAssistant(13,"Davit","Shengelia");
        Lecturer assistant12= management.createAssistant(14,"Ekaterine","Davitashvili");
        management.deleteAssistant(11);
        System.out.println("testing professor");
        Lecturer prof= management.createProfessor(1,"Giorgi","Kekelidze");
        Lecturer prof1= management.createProfessor(2,"Gega","Dzneladze");
        Lecturer prof2= management.createProfessor(3,"Marika","Garibashvili");
        Lecturer prof3= management.createProfessor(4,"Eter","Khukhashvili");
        management.deleteProfessor(3);
        System.out.println("assisgn assistant and professor to course");
        management.asignAssistantToCourse(assistant10,course3);
        management.asignProfessorToCourse(prof,course3);
        management.asignAssistantToCourse(assistant12,course4);
        management.asignProfessorToCourse(prof1,course4);
        management.asignAssistantToCourse(assistant12,course5);
        management.asignProfessorToCourse(prof1,course5);
        System.out.println("course 3 "+ course3);
        System.out.println("course 4 " +course4);
        System.out.println("course 5 "+course5);

        System.out.println("adding students to courses");
        management.addStudentToCourse(student1,course3);
        management.addStudentToCourse(student8,course3);
        management.addStudentToCourse(student6,course3);
        Student[] array={student4,student6,student7,student8};
        management.addStudentsToCourse(array,course4);
        management.addStudentsToCourse(array,course5);
        System.out.println("course 3 "+ course3);
        System.out.println("course 4 " +course4);
        System.out.println("course 5 "+course5);
        System.out.println("remove students from course");
        management.removeStudentFromCourse(student7,course5);
        management.removeStudentFromCourse(student4,course4);





    }


}

